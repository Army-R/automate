from openpyxl import load_workbook
from openpyxl.chart import BarChart, Reference

# Create the workbook
wb = load_workbook("piv_tbl.xlsx")

# Select the tab to work with
sheet = wb["survivors"]

# Select the rows and columns
min_col = wb.active.min_column
max_col = wb.active.max_column
min_row = wb.active.min_row
max_row = wb.active.max_row

# Instantiate a Barchart object
bc = BarChart()

# Locate data and categories
data = Reference(sheet, min_col=min_col+1, max_col=max_col, min_row=min_row, max_row=max_row)
categories = Reference(sheet, min_col=min_col, max_col=min_col, min_row=min_row+1, max_row=max_row)

# Adding data and categories
bc.add_data(data, titles_from_data=True)
bc.set_categories(categories)

# Make chart
sheet.add_chart(bc, "F5") #where to put the barchart
bc.title = "survivors"
bc.style = 1

# Save the results in a xlsx file
wb.save("barchart.xlsx")
