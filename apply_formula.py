from openpyxl import load_workbook
from openpyxl.utils import get_column_letter
# Create the workbook
wb = load_workbook("barchart.xlsx")

# Select the tab to work with
sheet = wb["survivors"]

# Select the rows and columns
min_col = wb.active.min_column
max_col = wb.active.max_column
min_row = wb.active.min_row
max_row = wb.active.max_row

# Add formula manually way for male
sheet["C893"] = "=SUM(C2:C892)"
sheet["C893"].style = "Good"

# Add formula with a for loop for female
for i in range(min_col+1, max_col):
    letter = get_column_letter(i)
    sheet[f"{letter}{max_row+1}"] = f"=SUM({letter}{min_row+1}:{letter}{max_row})"
    sheet[f"{letter}{max_row+1}"].style = "Good"

# Save the file
wb.save("formula.xlsx")