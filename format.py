from openpyxl import load_workbook
from openpyxl.styles import Font

# Create the workbook
wb = load_workbook("formula.xlsx")

# Select the tab to work with
sheet = wb["survivors"]

# Add a title and a subtitle
sheet["I1"] = "Titanic"
sheet["I2"] = "Survivors"

# Edit the font
sheet["I1"].font = Font("Norasi", bold=True, size=26)
sheet["I2"].font = Font("Norasi", italic=True, size=21)

# Save the document
wb.save("format.xlsx")