# Exract tables from PDFs

import camelot

# PDF URL
pdf = "https://arxiv.org/pdf/2010.14812.pdf"

# Read a PDF
tables = camelot.read_pdf(pdf, pages="22", flavor="stream")

# Export the table to a csv file
tables.export("pdf.csv", f="csv", compress=True)
tables[0].to_csv("pdf.csv")