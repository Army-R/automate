import pandas as pd

# Create a dataframe of the Excel file
df = pd.read_excel("Titanic.xlsx")

# Select which columns want to display
df = df[["Sex", "Name", "Survived"]]

# Create the pivot table
piv_tbl = df.pivot_table(index="Name", columns="Sex", values="Survived")

# Import the table to an Excel file
piv_tbl.to_excel("piv.tbl.xlsx", "survivors")
