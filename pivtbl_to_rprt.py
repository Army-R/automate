from openpyxl import load_workbook
from openpyxl.chart import BarChart, Reference
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font
import datetime

# Adding date to the filename
now = datetime.datetime.now()
month = now.strftime("%b")

# Create the workbook
wb = load_workbook("piv_tbl.xlsx")

# Select the tab to work with
sheet = wb["survivors"]

# Select the rows and columns
min_col = wb.active.min_column
max_col = wb.active.max_column
min_row = wb.active.min_row
max_row = wb.active.max_row

# Instantiate a Barchart object
bc = BarChart()

# References for the Barchart
data = Reference(sheet, min_col=min_col+1, max_col=max_col, min_row=min_row, max_row=max_row)
categories = Reference(sheet, min_col=min_col, max_col=min_col, min_row=min_row+1, max_row=max_row)

# Adding data and categories
bc.add_data(data, titles_from_data=True)
bc.set_categories(categories)

# Make chart
sheet.add_chart(bc, "F5")  #where to put the barchart
bc.title = "survivors"
bc.style = 1

# Loop through columns with data
for i in range(min_col+1, max_col):
    letter = get_column_letter(i)
    female = sheet[f"{letter}{max_row+1}"] = f"=SUM({letter}{min_row+1}:{letter}{max_row})"
    sheet[f"{letter}{max_row + 1}"].style = "Good"  #add style
for i in range(min_col+2, max_col+1):
    letter = get_column_letter(i)
    male = sheet[f"{letter}{max_row+1}"] = f"=SUM({letter}{min_row+1}:{letter}{max_row})"
    sheet[f"{letter}{max_row + 1}"].style = "Bad"  #add style

# Add a title and a subtitle
sheet["I1"] = "Titanic"
sheet["I2"] = "Survivors"

# Edit the font
sheet["I1"].font = Font("Norasi", bold=True, size=26)
sheet["I2"].font = Font("Norasi", italic=True, size=21)

# Save the document
wb.save(f"report_{month}.xlsx")
