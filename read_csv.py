# Read a .csv from a URL with Pandas

import pandas as pd

# Reading 1 csv file from the website
covid19 = pd.read_csv("https://www.stats.govt.nz/assets/Uploads/Effects-of-COVID-19-on-trade/Effects-of-COVID-19-on-trade-At-15-December-2021-provisional/Download-data/effects-of-covid-19-on-trade-at-15-december-2021-provisional.csv")

# Showing dataframe
print("Before rename:\n", covid19)

# Rename columns
covid19.rename(columns={"Transport_Mode":"Vector"}, inplace=True) 
#inplace=True update the columns names 

# Show dataframe updated
print("Afer rename:\n", covid19)