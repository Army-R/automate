from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.firefox.service import Service
from webdriver_manager.firefox import GeckoDriverManager
import pandas as pd

url = "https://tusecreto.io/#/recent"

options = Options()
options.add_argument("--no-sandbox")

# Manual way to call the driver
#path = "/home/armando/Documents/geckodriver-v0.33.0-linux64/geckodriver"
#service = Service(executable_path=path)

# Automatic way to call the driver
service = Service(GeckoDriverManager().install())

driver = webdriver.Firefox(service=service, options=options)

driver.get(url)

# Find elements
secrets = driver.find_elements(by="xpath", value="//div[@class='secret-body']").text

# Append the secret element to the secret list
secret.append(secrets)

# Create a dataframe
secret_dict = {"Secret": secret}
tuSecreto = pd.DataFrame(secret_dict)
tuSecreto.to_csv("tuSecreto")