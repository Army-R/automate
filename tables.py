import pandas as pd

# Return a list with multiple tables
simpsons = pd.read_html("https://en.wikipedia.org/wiki/List_of_The_Simpsons_episodes_(seasons_1%E2%80%9320)#Season_1_(1989%E2%80%9390)")

# How many tables are: 
print("There are:", len(simpsons), "tables")

# Print only the second table
print("The 2nd table is\n", simpsons[1])