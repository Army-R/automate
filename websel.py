# SELIUM WITH BAVE-CHROMEDRIVER

from selenium import webdriver
import chromedriver_binary
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import pandas as pd
from datetime import datetime as dt
import os
import sys

# Get the path to the executable
app_path = os.path.dirname(sys.executable)

# Custumize the file name
now = dt.now()
date = now.strftime("%m%d%H%M") #format the time mmddHHMM

# Automatically download the driver
#from webdriver_manager.chrome import ChromeDriverManager
#from webdriver_manager.chrome import ChromeType

# Webpage to test
url = "https://tusecreto.io/#/recent"

# Options
options = webdriver.ChromeOptions()
options.binary_location = "/usr/bin/brave-browser" #brave-browser path
options.add_argument("--no-sandbox")
options.add_argument("--disable-notifications")
options.add_argument("--disable-gpu")
options.add_argument("--headless") #run brave in the backgound

# Manual way to call the driver
path = "/home/armando/Documents/chromedriver-linux64/chromedriver"
service = Service(executable_path=path)

# Automatic way to call the driver
#service = Service(ChromeDriverManager(chrome_type=ChromeType.BRAVE).install())

# Run the driver
driver = webdriver.Chrome(service=service, options=options)
driver.get(url)

# Wait for the web page to load
wait = WebDriverWait(driver, 10)
wait.until(EC.presence_of_element_located((By.XPATH, "//div[@class='secret-body']")))

# Find female secrets
containers = driver.find_elements(by="xpath", value="//div[@class='secret  secret-2 ']")

# Create an empty list to store the secrets
secrets = []

# Iterate through the found element and append their text to the secret list
for container in containers:
    secret = container.find_element(by="xpath", value="./div[@class='secret-body']").text
    secrets.append(secret)

# For test purpose
#print(secrets)

# Create a dataframe
secret_dict = {"Secret": secrets}
tuSecreto = pd.DataFrame(secret_dict)
file_name = f"tuSecreto{date}.csv"
file_path = os.path.join(app_path, file_name) #concatenate
tuSecreto.to_csv(file_path)

# Exit the program
driver.quit()

