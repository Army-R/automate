import pywhatkit

# Variable to keep running the program
run = True

# Select the option
while run:
    print("""
        1- Send message to a contact
        2- Send message to a group 
        3- Exit    
        """)

    # Type the option
    option = int(input("Select an option: "))

    # Condition for the options
    if option == 1:
        # Gather user input:
        contact = input("Enter phone number: ")
        message = input("Write the message: ")
        hour = int(input("Set the hour: "))
        minute = int(input("Set the minutes: "))
        # Send message to contacts
        pywhatkit.sendwhatmsg(contact, message, hour, minute, 15, True, 2)
    elif option == 2:
        # Gather user input:
        group = input("Enter group ID: ")
        message = input("Write the message: ")
        hour = int(input("Set the hour: "))
        minute = int(input("Set the minutes: "))
        # Send message to group
        pywhatkit.sendwhatmsg_to_group(group, message, hour, minute)
    elif option == 3:
        run = False
    else:
        print("Invalid option")



